package u04lab.polyglot.a05b

import scala.util.Random

/** solution and descriptions at https://bitbucket.org/mviroli/oop2019-esami/src/master/a05b/sol2/ */
class LogicsImpl(val gridSize: Int) extends Logics:

  private var tickCount: Int = 0
  private val random = Random()
  private val initial = (random.nextInt(gridSize - 2) + 1, random.nextInt(gridSize - 2) + 1)

  override def tick(): Unit =
    tickCount = tickCount + 1

  override def isOver: Boolean =
    initial._2  -tickCount < 0 || initial._2 + tickCount >= gridSize ||
      initial._1 - tickCount < 0 || initial._1 + tickCount >= gridSize

  override def hasElement(x: Int, y: Int): Boolean =
    x == initial._1 && Math.abs(y - initial._2) <= tickCount ||
      y == initial._2 && Math.abs(x - initial._1) <= tickCount ||
      (x - y == initial._1 - initial._2) && Math.abs(x - initial._1) <= tickCount ||
      (x + y == initial._1 + initial._2) && Math.abs(x - initial._1) <= tickCount


