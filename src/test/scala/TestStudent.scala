import u04lab.code.{Course, Student}
import org.junit.*
import org.junit.Assert.*

class TestStudent:

  @Test def testStudentObject() =
    val cPPS = Course("PPS", "Viroli")
    val cPCD = Course("PCD", "Ricci")
    val cSDR = Course("SDR", "D'Angelo")
    val s1 = Student("mario", 2015)
    val s2 = Student("gino", 2016)
    val s3 = Student("rino") // defaults to 2017
    s1.enrolling(cPPS, cPCD)
    s2.enrolling(cPPS)
    s3.enrolling(cPPS, cPCD, cSDR)
    assertEquals(s1.courses.toString, "Cons(PPS,Cons(PCD,Nil()))")
    assertEquals(true, s1.hasTeacher("Ricci"))
    assertEquals(s2.courses.toString, "Cons(PPS,Nil())")
    assertEquals(s3.courses.toString, "Cons(PPS,Cons(PCD,Cons(SDR,Nil())))")
