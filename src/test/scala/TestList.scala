import org.junit.*
import org.junit.Assert.*
import u04lab.code.{Course, Student}

class TestList:

  import u04lab.code.List.*

  @Test def testFactory() =
    assertEquals(Cons(1, Cons(2, Cons(3, Cons(4, Cons(5, Nil()))))), apply(1, 2, 3, 4, 5))
    assertEquals(Cons(true, Cons(false, Cons(true, Nil()))), apply(true, false, true))
    assertEquals(Nil(), apply())

